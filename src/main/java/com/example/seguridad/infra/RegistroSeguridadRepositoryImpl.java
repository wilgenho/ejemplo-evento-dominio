package com.example.seguridad.infra;

import com.example.seguridad.domain.RegistroSeguridad;
import com.example.seguridad.domain.RegistroSeguridadRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Repository;

@Repository
@Log4j2
public class RegistroSeguridadRepositoryImpl implements RegistroSeguridadRepository {
    @Override
    public void guardar(RegistroSeguridad registroSeguridad) {
        log.info("{} usuario {}", registroSeguridad.getAccion(), registroSeguridad.getUsuario());
    }
}
