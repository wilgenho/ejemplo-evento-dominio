package com.example.seguridad.listener;

import com.example.seguridad.service.RegistroSeguridadService;
import com.example.shared.domain.UsuarioDeshabilitado;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class UsuarioDeshabilitadoListener {

    private final RegistroSeguridadService registroSeguridadService;

    public UsuarioDeshabilitadoListener(final RegistroSeguridadService registroSeguridadService) {
        this.registroSeguridadService = registroSeguridadService;
    }

    @Async
    @EventListener
    public void onUsuarioDeshabilitado(UsuarioDeshabilitado usuarioDeshabilitado) {
        log.info("Usuario  {} deshabilitado", usuarioDeshabilitado.getNombre());
        registroSeguridadService.registrar(usuarioDeshabilitado.getNombre(), "deshabilitar");
    }
}
