package com.example.seguridad.domain;

public interface RegistroSeguridadRepository {
    void guardar(RegistroSeguridad registroSeguridad);
}
