package com.example.seguridad.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RegistroSeguridad {
    private LocalDateTime fecha;
    private String usuario;
    private String accion;

    public RegistroSeguridad(String usuario, String accion) {
        this.usuario = usuario;
        this.accion = accion;
        this.fecha = LocalDateTime.now();
    }
}
