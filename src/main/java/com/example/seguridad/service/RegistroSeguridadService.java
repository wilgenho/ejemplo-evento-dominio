package com.example.seguridad.service;

import com.example.seguridad.domain.RegistroSeguridad;
import com.example.seguridad.domain.RegistroSeguridadRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class RegistroSeguridadService {

    private final RegistroSeguridadRepository repository;

    public RegistroSeguridadService(final RegistroSeguridadRepository repository) {
        this.repository = repository;
    }

    public void registrar(String usuario, String accion) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            log.error(e);
        }
        repository.guardar(new RegistroSeguridad(usuario, accion));
    }
}
