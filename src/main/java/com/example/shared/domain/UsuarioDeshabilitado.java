package com.example.shared.domain;

import com.example.usuario.domain.Usuario;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UsuarioDeshabilitado extends DomainEvent {

    public static final String EVENT_NAME = "usuario.deshabilitado";

    private String id;
    private String nombre;
    private String email;

    public UsuarioDeshabilitado(String id, String nombre, String email) {
        super(id);
        this.id = id;
        this.nombre = nombre;
        this.email = email;
    }

    public static UsuarioDeshabilitado fromDomain(Usuario usuario) {
        return new UsuarioDeshabilitado(usuario.getId(), usuario.getNombre(), usuario.getEmail());
    }

    @Override
    public String eventName() {
        return EVENT_NAME;
    }
}
