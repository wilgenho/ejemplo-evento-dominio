package com.example.usuario.web;

import com.example.usuario.service.DeshabilitarUsuario;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeshabilitarUsuarioController {

    private final DeshabilitarUsuario deshabilitarUsuario;

    public DeshabilitarUsuarioController(final DeshabilitarUsuario usuarioService) {
        this.deshabilitarUsuario = usuarioService;
    }

    @PutMapping("/usuarios/{id}/deshabilitar")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void dehabilitar(@PathVariable("id") String id) {
        deshabilitarUsuario.deshabilitar(id);
    }

}
