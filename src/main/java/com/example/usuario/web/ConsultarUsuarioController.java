package com.example.usuario.web;

import com.example.usuario.service.ConsultarUsuario;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsultarUsuarioController {

    private final ConsultarUsuario consultarUsuario;

    public ConsultarUsuarioController(ConsultarUsuario consultarUsuario) {
        this.consultarUsuario = consultarUsuario;
    }

    @GetMapping("/usuarios/{id}")
    public ConsultarUsuarioResponse consultar(@PathVariable("id") String id) {
        return ConsultarUsuarioResponse.fromDomain(consultarUsuario.consultar(id));
    }
}
