package com.example.usuario.web;

import com.example.usuario.domain.Usuario;
import lombok.Data;

@Data
public class ConsultarUsuarioResponse {
    private String id;
    private String nombre;
    private String email;
    private Boolean activo;

    public static ConsultarUsuarioResponse fromDomain(Usuario usuario) {
        ConsultarUsuarioResponse response = new ConsultarUsuarioResponse();
        response.id = usuario.getId();
        response.nombre = usuario.getNombre();
        response.email = usuario.getEmail();
        response.activo = usuario.getActivo();
        return response;
    }

}
