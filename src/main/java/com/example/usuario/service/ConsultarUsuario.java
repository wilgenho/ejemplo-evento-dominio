package com.example.usuario.service;

import com.example.usuario.domain.Usuario;
import com.example.usuario.domain.UsuarioRepository;
import org.springframework.stereotype.Service;

@Service
public class ConsultarUsuario {

    private final UsuarioRepository usuarioRepository;

    public ConsultarUsuario(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    public Usuario consultar(final String id) {
        return usuarioRepository.consultar(id);
    }

}
