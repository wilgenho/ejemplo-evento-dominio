package com.example.usuario.service;

import com.example.seguridad.service.RegistroSeguridadService;
import com.example.shared.domain.EventBus;
import com.example.usuario.domain.Usuario;
import com.example.usuario.domain.UsuarioRepository;
import org.springframework.stereotype.Service;

@Service
public class DeshabilitarUsuario {

    private final UsuarioRepository usuarioRepository;
    private final EventBus eventBus;

    public DeshabilitarUsuario(UsuarioRepository usuarioRepository, EventBus eventBus) {
        this.usuarioRepository = usuarioRepository;
        this.eventBus = eventBus;
    }

    public void deshabilitar(final String id) {
        Usuario usuario = usuarioRepository.consultar(id);
        if (usuario.deshabilitar()) {
            usuarioRepository.guardar(usuario);
            eventBus.publish(usuario.pullDomainEvents());
        }
    }

}
