package com.example.usuario.infra;

import com.example.usuario.domain.Usuario;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioEntity {
    @Id
    private String id;
    private String nombre;
    private String email;
    private Boolean activo;

    public Usuario toDomain() {
        return new Usuario(id, nombre, email, activo);
    }

    public static UsuarioEntity fromDomain(Usuario usuario) {
        return new UsuarioEntity(usuario.getId(), usuario.getNombre(), usuario.getEmail(), usuario.getActivo());
    }
}
