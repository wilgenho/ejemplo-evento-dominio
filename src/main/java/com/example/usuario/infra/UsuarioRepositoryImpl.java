package com.example.usuario.infra;

import com.example.usuario.domain.Usuario;
import com.example.usuario.domain.UsuarioRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public class UsuarioRepositoryImpl implements UsuarioRepository {

    private final UsuarioRepositoryJPA repositoryJPA;

    public UsuarioRepositoryImpl(UsuarioRepositoryJPA repositoryJPA) {
        this.repositoryJPA = repositoryJPA;
    }

    @Override
    public Usuario consultar(String id) {
        return repositoryJPA.findById(id).get().toDomain();
    }

    @Override
    public void guardar(Usuario usuario) {
        repositoryJPA.save(UsuarioEntity.fromDomain(usuario));
    }
}

interface UsuarioRepositoryJPA extends JpaRepository<UsuarioEntity, String> {}
