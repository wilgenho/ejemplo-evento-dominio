package com.example.usuario.domain;

import com.example.shared.domain.AggregateRoot;
import com.example.shared.domain.UsuarioDeshabilitado;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Usuario extends AggregateRoot {
    private String id;
    private String nombre;
    private String email;
    private Boolean activo;


    public boolean deshabilitar() {
        if(activo) {
            activo = false;
            record(UsuarioDeshabilitado.fromDomain(this));
            return true;
        }
        return false;
    }
}
