package com.example.usuario.domain;

public interface UsuarioRepository {
    Usuario consultar(String id);
    void guardar(Usuario usuario);
}
