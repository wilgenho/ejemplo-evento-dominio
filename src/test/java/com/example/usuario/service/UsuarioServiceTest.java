package com.example.usuario.service;

import com.example.shared.domain.DomainEvent;
import com.example.shared.domain.EventBus;
import com.example.usuario.domain.Usuario;
import com.example.usuario.domain.UsuarioMother;
import com.example.usuario.domain.UsuarioRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
class UsuarioServiceTest {

    @InjectMocks
    DeshabilitarUsuario deshabilitarUsuario;

    @Mock
    UsuarioRepository usuarioRepository;

    @Mock
    EventBus eventBus;

    @Captor
    ArgumentCaptor<List<DomainEvent>> eventosCaptor;

    @Test
    @DisplayName("Debería deshabilitar un usuario que está habilitado")
    void deberiaDeshabilitarUsuarioHabilitado() {
        Usuario usuarioHabilitado = UsuarioMother.habilitado();
        Usuario usuarioDeshabilitado = UsuarioMother.deshabilitado();
        String id = usuarioHabilitado.getId();
        String nombre = usuarioHabilitado.getNombre();

        given(usuarioRepository.consultar(id)).willReturn(usuarioHabilitado);
        willDoNothing().given(usuarioRepository).guardar(any(Usuario.class));
        willDoNothing().given(eventBus).publish(eventosCaptor.capture());

        deshabilitarUsuario.deshabilitar(id);

        then(usuarioRepository).should().consultar(id);
        then(usuarioRepository).should().guardar(usuarioDeshabilitado);
        then(eventBus).should().publish(any());
        assertEquals(1, eventosCaptor.getValue().size());
        assertEquals("usuario.deshabilitado", eventosCaptor.getValue().get(0).eventName());
    }

    @Test
    @DisplayName("No debería deshabilitar un usuario que ya está deshabilitado")
    void noDeberiaDeshabilitarUsuarioDeshabilitado() {
        Usuario usuarioDeshabilitado = UsuarioMother.deshabilitado();
        String id = usuarioDeshabilitado.getId();

        given(usuarioRepository.consultar(id)).willReturn(usuarioDeshabilitado);

        deshabilitarUsuario.deshabilitar(id);

        then(usuarioRepository).should().consultar(id);
        then(usuarioRepository).shouldHaveNoMoreInteractions();
        then(eventBus).shouldHaveNoMoreInteractions();
    }

}