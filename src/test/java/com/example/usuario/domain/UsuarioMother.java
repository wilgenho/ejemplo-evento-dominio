package com.example.usuario.domain;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class UsuarioMother {

    private UsuarioMother() {}

    public static Usuario habilitado() {
        return new Usuario("f5a2e597-5d88-4146-9009-69ac79430133", "usuario", "usuario@email.com", true);
    }

    public static Usuario deshabilitado() {
        return new Usuario("f5a2e597-5d88-4146-9009-69ac79430133", "usuario", "usuario@email.com", false);
    }

}